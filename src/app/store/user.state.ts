import { State, Action, StateContext, Selector } from '@ngxs/store';

export class SetToken {
	static readonly type = '[User] Set Token';
	constructor(public token: string) { }
}

export class SetNickname {
	static readonly type = '[User] Set Nickname';
	constructor(public nickname: string) { }
}

export class SetProfileId {
	static readonly type = '[User] Set Id';
	constructor(public id: string) { }
}

// export class SetChats {
// 	static readonly type = '[User] Set Token';
// 	constructor(public token: string) { }
// }

export interface UserStateModel {
	token: string;
	nickname: string;
	id: string;
}

@State<UserStateModel>({
	name: 'timetable',
	defaults: {
		token: '',
		nickname: '',
		id: ''
	}
})

export class UserState {
	@Selector()
	static token(state: UserStateModel) { return state.token; }

	@Selector()
	static username(state: UserStateModel) { return state.nickname; }

	@Selector()
	static profileId(state: UserStateModel) { return state.id; }

	@Action(SetToken)
	private setToken({ patchState }: StateContext<UserStateModel>, { token }: SetToken) {
		patchState({
			token
		});
	}

	@Action(SetNickname)
	private setNickname({ patchState }: StateContext<UserStateModel>, { nickname }: SetNickname) {
		patchState({
			nickname
		});
	}

	@Action(SetProfileId)
	private setProfileId({ patchState }: StateContext<UserStateModel>, { id }: SetProfileId) {
		patchState({
			id
		});
	}

}
