import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesProfileComponent } from './messages-profile.component';

describe('MessagesProfileComponent', () => {
  let component: MessagesProfileComponent;
  let fixture: ComponentFixture<MessagesProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
