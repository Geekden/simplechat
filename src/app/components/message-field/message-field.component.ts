import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-message-field',
  templateUrl: './message-field.component.html',
  styleUrls: ['./message-field.component.scss']
})
export class MessageFieldComponent implements OnInit {
  message: string;

  // isEmojiInit: boolean = false;

  constructor() { }

  ngOnInit() {
    const emojiInit = setInterval(() => {
      Array.from(document.getElementsByClassName('emoji')).forEach(el => {
        console.log('1');
        el.addEventListener('click', (e) => {
          clearInterval(emojiInit);
          console.log(e.target);
        });
      });

    }, 500);
  }

  addEmoji(emoji: string) {
    console.log('object');
    this.message += `:${emoji}:`;
  }

}