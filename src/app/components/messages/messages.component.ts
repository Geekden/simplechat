import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { UserState, UserStateModel } from '../../store/user.state';
import { Observable } from 'rxjs';
import { UserService } from './../../providers/user.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {
@Select(UserState.token)
  public token$: Observable<string>;

  constructor(private store: Store) {

  }

  ngOnInit() {
  }

}
