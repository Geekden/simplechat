import { Component, OnInit } from '@angular/core';
import { UserService } from '../../providers/user.service';
import { Store, Select } from '@ngxs/store';
import { UserState, UserStateModel } from '../../store/user.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @Select(UserState.username)
  public username$: Observable<string>;

  @Select(UserState.profileId)
  public id$: Observable<string>;

  username: string;
  id: string;

  constructor(private user: UserService, private store: Store) {
    this.username$.subscribe((val) => {
      this.username = val;
    });

    this.id$.subscribe((val) => {
      this.id = val;
    });
  }

  ngOnInit() {
  }

  logout() {
    this.user.logout();
  }

}
