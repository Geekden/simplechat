import { Store, Select } from '@ngxs/store';
import { UserState, UserStateModel, SetToken } from '../../store/user.state';
import { Observable } from 'rxjs';
import { UserService } from './../../providers/user.service';
import { Component, OnInit } from '@angular/core';

interface RegistrationResponse {
  type: string;
  message?: string;
  token?: string;
}

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  @Select(UserState.token)
  public token$: Observable<string>;

  email: string;
  nick: string;
  password: string;
  error: string;

  isLogin: boolean = true;

  isAuthed: boolean = false;

  constructor(private user: UserService, private store: Store) {
    this.token$.subscribe((val) => {
      console.log(val);
      this.isAuthed = val.length > 0;
    });
  }

  ngOnInit() {
    // this.user.registrationResponseChange.subscribe((val: RegistrationResponse) => {
    //   if (val.type === 'success') {
    //     this.isAuthed = true;
    //   } else if (val.type === 'error') {
    //     this.error = val.message;
    //   }
    // });
  }

  changeType() {
    this.isLogin = !this.isLogin;

    this.email = '';
    this.nick = '';
    this.password = '';
    this.error = '';
  }

  validateEmail(): boolean {
    if (this.email) this.email = this.email.trim();

    if (!this.email || this.email.length === 0) {
      this.error = 'Введите E-mail';
      return false;
    }

    if (!/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/.test(this.email)) {
      this.error = 'Введите корректный E-mail';
      return false;
    }

    return true;
  }

  validateNickname(): boolean {
    if (this.nick) this.nick = this.nick.trim();

    if (!this.nick || this.nick.length === 0) {
      this.error = 'Введите Никнейм';
      return false;
    }

    return true;
  }

  validatePassword(): boolean {
    if (this.password) this.password = this.password.trim();

    if (!this.password || this.password.length === 0) {
      this.error = 'Введите Пароль';
      return false;
    }

    if (this.password.length < 8) {
      this.error = 'Длина пароля должна быть не менее 8 символов';
      return false;
    }

    return true;
  }

  register() {
    if (this.validateEmail() && this.validateNickname() && this.validatePassword()) {
      this.user.register(this.email, this.nick, this.password);
    }
  }

  login() {
    if (this.validateEmail() && this.validatePassword()) {
      this.user.login(this.email, this.password);
    }
  }

}
