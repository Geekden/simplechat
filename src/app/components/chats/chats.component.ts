import { Component, OnInit } from '@angular/core';
import { UserService } from './../../providers/user.service';

interface ApiResponse {
  type: string;
  message?: string;
  token?: string;
}

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss']
})
export class ChatsComponent implements OnInit {
  newChatId: string = '';

  fieldFocus: boolean = false;

  constructor(private user: UserService) { }

  ngOnInit() {
  }

  toggleField() {
    // document.getElementById('chats__add-id').focus();
    // if (this.fieldFocus) {
    if (this.newChatId.length > 0) {
      this.user.addChat(this.newChatId);
    }

    // this.user.addChat(this.newChatId).subscribe((data: ApiResponse) => {
    //   if (data.type === 'error') {
    //     document.getElementById('chats__add-id').style.borderColor = '#d7a2a8';
    //   }
    // });
    // }
    // this.fieldFocus = true;
  }
}
