import 'reflect-metadata';
import '../polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule, HttpClient } from '@angular/common/http';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ElectronService } from './providers/electron.service';
import { UserService } from './providers/user.service';

import { WebviewDirective } from './directives/webview.directive';

import { NgxsModule } from '@ngxs/store';
import { NgxsDispatchPluginModule } from '@ngxs-labs/dispatch-decorator';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { UserState } from './store/user.state';

import { TippyModule } from 'ng-tippy';

import { AppComponent } from './app.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { ChatsComponent } from './components/chats/chats.component';
import { MessagesComponent } from './components/messages/messages.component';
import { MessageFieldComponent } from './components/message-field/message-field.component';
import { ProfileComponent } from './components/profile/profile.component';
import { MessagesProfileComponent } from './components/messages-profile/messages-profile.component';
import { ChatComponent } from './components/chat/chat.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    WebviewDirective,
    RegistrationComponent,
    ChatsComponent,
    MessagesComponent,
    MessageFieldComponent,
    ProfileComponent,
    MessagesProfileComponent,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgxsModule.forRoot([UserState]),
    NgxsStoragePluginModule.forRoot(),
    NgxsDispatchPluginModule.forRoot(),
    TippyModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],
  providers: [ElectronService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
