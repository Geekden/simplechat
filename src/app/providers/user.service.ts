import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Store, Select } from '@ngxs/store';
import { UserState, UserStateModel, SetToken, SetNickname, SetProfileId } from '../store/user.state';
import { Observable } from 'rxjs';

interface ApiResponse {
  type: string;
  message?: string;
  token?: string;
  username?: string;
  id?: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiIp: string = 'http://95.79.50.190:5000';

  @Select(UserState.token)
  public token$: Observable<string>;
  // @Select(UserState)
  // public user$: Observable<UserStateModel>;

  // token: string;
  // chats: any;

  // registrationResponseChange: Subject<Object> = new Subject<Object>();

  constructor(private http: HttpClient, private store: Store) {
    this.token$.subscribe((val) => {
      if (val.length > 0) {
        this.getUserInfo();
        this.getChats();
      }
    });
  }

  async register(email: string, nickname: string, password: string) {
    this.http.get(`${this.apiIp}/api/registration/mail=${email}&nick=${nickname}&password=${password}`).subscribe((data: ApiResponse) => {
      console.log(data);
      if (data.type === 'success') {
        this.store.dispatch(new SetToken(data.token));
      }
    });
  }

  async login(email: string, password: string) {
    this.http.get(`${this.apiIp}/api/authorization/mail=${email}&password=${password}`).subscribe((data: ApiResponse) => {
      console.log(data);
      if (data.type === 'success') {
        this.store.dispatch(new SetToken(data.token));
      }
    });
  }

  addChat(id: string) {
    const token = this.store.selectSnapshot(UserState.token);
    // const request = this.http.get(`${this.apiIp}/api/chats/add/token=${token}&id=${id}`);
    // request.subscribe(() => {
    //   this.getChats();
    // });
    // return request;

    this.http.get(`${this.apiIp}/api/chats/add/token=${token}&id=${id}`).subscribe(() => {
      this.getChats();
    });
  }

  async getChats() {
    const token = this.store.selectSnapshot(UserState.token);
    this.http.get(`${this.apiIp}/api/chats/getallchats/token=${token}`).subscribe((data) => {
      console.log(data);
      // this.store.dispatch(new SetChats(token));
    });
  }

  async getUserInfo() {
    const token = this.store.selectSnapshot(UserState.token);
    this.http.get(`${this.apiIp}/api/get/info/token=${token}/username&id`).subscribe((data: ApiResponse) => {
      console.log(data);
      if (data.type === 'success') {
        this.store.dispatch(new SetNickname(data.username));
        this.store.dispatch(new SetProfileId(data.id));
      }
    });
  }

  async logout() {
    this.store.dispatch(new SetToken(''));
  }
}
